require 'nokogiri'

module Nesta
  module Plugin
    module Toc
      module Helpers
        # If your plugin needs any helper methods, add them here...
      end
    end
  end

  class Page

    def to_html(scope = Object.new)
      html = super(scope)
      doc = Nokogiri("<html>#{html}</html>")
      add_anchors(doc)
      doc.to_html
    end

    def add_anchors(doc)
      doc.css('h1, h2, h3').each do |h|
        h.before("<a id=\"#{h.text.gsub(' ', '-')}\" href=\"#\"></a>")
      end
    end

    def table_of_contents
      puts "*** nesta-plugin-toc table_of_contents for #{path}"
      doc = Nokogiri("<html>#{to_html(self)}</html>")
      headers = doc.css('h1, h2, h3')
      toc = '<div class="contents">'
      toc += '  <ul>'
      # TODO improve indentation with nested lists
      current_level = 1
      headers.each do |h|
        level = h.name.chars.last.to_i
        indent = '&nbsp;&nbsp;' * (level - 1)
        # build the toc list
        toc += "    <li>#{indent}#{'<strong>' if level == 1}<a href=\"##{h.text.gsub(' ', '-')}\">#{h.text}</a>#{'</strong>' if level == 1}</li>"
        current_level = level
      end
      toc += '  </ul>'
      toc += '</div>'
      toc
    end
  end
end
