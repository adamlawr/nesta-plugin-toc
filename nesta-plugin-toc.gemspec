# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "nesta-plugin-toc/version"

Gem::Specification.new do |spec|
  spec.name        = "nesta-plugin-toc"
  spec.version     = Nesta::Plugin::Toc::VERSION
  spec.authors     = ["adam lawrence"]
  spec.email       = ["adam@raceweb.ca"]
  spec.homepage    = "https://gitlab.com/adamlawr/nesta-plugin-toc"
  spec.summary     = %q{add per page table of contents to a nesta project}
  spec.description = %q{add per page table of contents to a nesta project}
  spec.license     = "MIT"

  spec.rubyforge_project = "nesta-plugin-toc"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  # specify any dependencies here; for example:
  # spec.add_development_dependency "rspec"
  # spec.add_runtime_dependency "rest-client"
  spec.add_runtime_dependency 'nesta', '~> 0.9', '>= 0.9.11'
  spec.add_runtime_dependency 'nokogiri', '~> 1.8', '>= 1.8.4'
  spec.add_development_dependency 'rake', '~> 0'
  spec.add_development_dependency 'pry', '~> 0.11.3'
  spec.add_development_dependency 'pry-byebug', '~> 3.6', '>= 3.6.0'
end
