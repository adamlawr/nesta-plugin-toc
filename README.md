# readme
Adds a table of contents built from headers for any page in a [Nesta](http://nestacms.com) project.

## installation
To use this plugin just add it to your Nesta project's `Gemfile`:

  gem 'nesta-plugin-toc', :git => 'git@gitlab.com:adamlawr/nesta-plugin-toc.git'

and then install it with Bundler:

  $ bundle

## usage
add a reference to `@page.table_of_contents` somewhere in your view file.
for example: `page.haml`

    .sidebar
      ~ @page.table_of_contents
      = haml :page_meta, :layout => false, :locals => { :page => @page }
